import { Component, OnInit } from '@angular/core';
import { Weather } from '../interfaces/weather';
import { Observable } from 'rxjs';
import { TempService } from '../temp.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  
  temperature;
  image; 
  city;
  tempData$:Observable<Weather>;
  errorMessage:string; 
  hasError:boolean = false; 
  likes:number = 0;
  

  //פונקציה שסופרת את כמות הלייקים שנעשו
  addLikes(){
    this.likes ++ //likes that belongs to this section...
  }

  constructor(private route: ActivatedRoute, private tempService:TempService ) { }

  ngOnInit() {
  
  this.city= this.route.snapshot.params.city;
  this.tempData$ = this.tempService.searchWeatherData(this.city);
  this.tempData$.subscribe(
    data => {
      console.log(data);
      this.temperature = data.temperature;
        this.image = data.image;


    } 
  )

}

}