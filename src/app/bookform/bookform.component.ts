import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {
  
  title:string;
  author:string;
  buttonText:string = 'Add book' 

  constructor(private booksservice:BooksService, 
    private router:Router
) { }

onSubmit(){
  this.booksservice.addBook(this.title, this.author)
  this.router.navigate(['/books']);
}

  ngOnInit() {
  }

}
