import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  //המשתנה שיחזיק את הספרים
 // books:any;
  panelOpenState = false;
  books$:Observable<any>;

  constructor(private booksservice:BooksService) { }

  ngOnInit() {
    //בטעינת הדף נקרא לפונקציה שמושכת את הספרים לכאן
      this.books$ = this.booksservice.getBooks();
    //  this.booksservice.addBooks();
    //הרשמה ל observable
    // this.books = this.booksservice.getBooks().subscribe(
    //   (books) => this.books = books
    // )

  }

}
