import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  title:string;
  author:string;

  books:any = [{title:'Alice in Wonderland', author:'Lewis Carrol'},
  {title:'War and Peace', author:'Leo Tolstoy'}, 
  {title:'The Magic Mountain', author:'Thomas Mann'}, 
  {title:'A Song Of Ice And Fire', author:'George R R Martin'}]
  

  
  //  פונקציה שמטרתה לשלוף ספרים מתוך מסד הנתונים בהמשך
  //  getBooks(){
  //    return this.books;
  // }


  //כתיבה אחרת של הפונקציה באמצעות arrowfunction
  //הפונקציה מראה את הספרים שהזנו ב VS
//  getBooks(){
//     const booksObservable = new Observable(
//       observer => {
//         setInterval(
//           () => observer.next(this.books),5000
//         )
//       }
//     )
//     return booksObservable;

//   }

  //setInterval- נשתמש בפונקציה זו כדי לגרום לכך שכל 5 שניות יתווסף ספר חדש
  // addBooks(){
  //   setInterval(
  //     () => this.books.push({title:'A new book', author: 'New Author'})
  //   ,5000)
  // }

  //ביצוע משיכת הנתונים מה db
  getBooks():Observable<any[]>{
    return this.db.collection('books').valueChanges();
  }

  addBook(title:string, author:string){
    const book = {title:title, author:author}
    this.db.collection('books').add(book);
  }
  
  

//מאתחלים תכונה כדי למשוך נתונים מה db
  constructor(private db:AngularFirestore) { }
}
