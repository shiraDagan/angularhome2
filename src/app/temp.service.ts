import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TempService {

//ה קישור יכיל את כתובת ה API
private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
 //שני המשתנים הבאים יכילו נתונים שיתקבלו מהאתר הספציפי ממנו נלקחה כתובת ה API
 private KEY = "2e1ee83bb3ac24c90a0d728a2490768b ";
 private IMP = "&units=metric";

  //map- פונקצייה שמתרגמת את הנתונים מפורמט של מזג אוויר- שורה לפורמט של מזג אאויר 
  searchWeatherData(cityName:String): Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(map(data=>this.transformWeatherData(data))
    )

  }

  private transformWeatherData(data:WeatherRaw):Weather{
    return {
      name:data.name,
      country:data.sys.country,
      image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon,

    }

  }



  
  constructor(private http:HttpClient) { }
}
