export interface WeatherRaw {
    
//Weather- מערך של אובייקטים שיש בו description ו icon

    weather:[
        { 
           description:String,
           icon:String
        }
     ];
     //Main- אובייקט שיש בו משתנה temp
     main:{ 
        temp:number,
     };
     //sys- אובייקט שיש בו משתנה country
     sys:{ 
        country:String,
     };
     coord:{
         lon:number,
         lat:number,
     }
     name:String,

}
