// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig: {
    apiKey: "AIzaSyB6eusGmU4g-_LfBpfqrbpJ6FtMZ_2FjBM",
    authDomain: "angularhome2.firebaseapp.com",
    databaseURL: "https://angularhome2.firebaseio.com",
    projectId: "angularhome2",
    storageBucket: "angularhome2.appspot.com",
    messagingSenderId: "316921909876",
    appId: "1:316921909876:web:9a04eb4552407d83587721",
    measurementId: "G-BV4VJJZY6Q"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
